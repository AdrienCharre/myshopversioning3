package test.com.ci.myshop.model;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;
import com.ci.myshop.model.Item;
public class ItemTest {
   @Test
   public void setName() {
       Item item= new Item("ob", 2, 100, 50);
       item.setName("doe");
       assert("doe".equals(item.getName()));
   }
}