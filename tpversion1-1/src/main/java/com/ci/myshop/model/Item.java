package com.ci.myshop.model;

import java.util.Scanner;

public class Item {

	
	private String name;
	private int id;
	private float price;
	private int nbrElt;

	public Item() {this.name ="";}
	
	public Item (String name, int id, float i, int nbrElt) {
		this.name = name;
		this.id = id;
		this.price = i;
		this.nbrElt = nbrElt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public int getNbrElt() {
		return nbrElt;
	}

	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}


	public String display() {
		System.out.println("Entrez le nom du produit");
		Scanner scanner = new Scanner(System.in);
		name = scanner.next();
		scanner.close();
		return name;
	}
}








