package com.ci.myshop.controller;

import java.util.ArrayList;
import java.util.List;
import com.ci.myshop.model.Item;

public class Storage  {
	private List <Item> items = new ArrayList<Item>();
	public Storage(List<Item> items) {
		this.items = items;
	}

	public Item getItem(String name){
		for(int i = 0 ; i < this.items.size(); i++) {
			Item a = this.items.get(i);
			if (a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}
}
