package com.ci.myshop.controller;

import com.ci.myshop.model.Item;

public class Shop {

	private	Storage storage;
	private	float cash;

	public Shop (Storage storage, float cash) {
		this.storage = storage ;
		this.cash = cash;
	}

	public void sell(String name) {
		Item item = this.storage.getItem(name);
		if (item.getName().contentEquals("")) {
			System.out.println("Erreur !");
		}
		int newStock = item.getNbrElt() - 1;
		item.setNbrElt(newStock);
		this.cash += item.getPrice();
	}

	public boolean buy(Item item) {
		// récupérer l'élement dans la liste, pour vérifier si on a pas déja l'item dans le storage
		int itemPosition = this.storage.getItems().indexOf(item);
		float itemPrice = item.getPrice();

		// quand = -1, cela veut dire que l'élément n'existe pas
		if (itemPosition != -1){
			// on check si le shop a assez de cash pour acheter l'item
			if (this.cash - itemPrice >= 0) {
				int newStock = item.getNbrElt() + 1;

				// on remplace l'item existant avec les nouvelles infos de stock
				Item itemWithNewStock = new Item(
						item.getName(),
						item.getId(),
						itemPrice,
						newStock
						);

				// permet de remplacer l'item à la position donnée
				this.storage.getItems().set(itemPosition, itemWithNewStock);
				// on dépense le montant de l'item
				this.cash -= itemPrice;

				// on retourne true, car l'achat est ok
				return true;
			} else {
				// on retourne false, car l'achat est ko parce que pas assez de sous
				return false;
			}
		} else {
			// l'item n'existe pas
			// on vérifie qu'on peut l'acheter
			if (this.cash - itemPrice >= 0) {
				// on l'ajoute au storage puisque c'est un nouvel item
				this.storage.getItems().add(item);

				// on dépense le montant de l'item
				this.cash -= itemPrice;

				// on retourne true, car l'achat est ok
				return true;
			} else {
				// on retourne false, car l'achat est ko parce que pas assez de sous
				return false;
			}
		}
	}

	public float getCash() {
		return this.cash;
	}
}

