package com.ci.myshop;

import java.util.ArrayList;
import java.util.List;

import com.ci.myshop.controller.Shop;
import com.ci.myshop.controller.Storage;
import com.ci.myshop.model.Item;

public class Launch {
	public static void main (String[] args){
		Item item1 = new Item("Vodka", 1, 2, 10);
		Item item2 = new Item("Wiskey", 2, 3, 11);

		List<Item> items = new ArrayList<Item>();
		items.add(item1);
		items.add(item2);

		Storage storage = new Storage(items);
		Shop magasin = new Shop(storage, 150);
		
		// Vente
		System.out.println("VENTE");
		System.out.println("Cash dans le magasin avant vente : " + magasin.getCash());
		magasin.sell("Vodka");
		System.out.println("Cash dans le magasin après vente : " + magasin.getCash());
		System.out.println();

		// Achat
        Item item3 = new Item("vin", 12, 15, 5);
		System.out.println("ACHAT");
        // pour un item déjà existant dans le storage
        boolean achatOkPourItemA = magasin.buy(item1);
        if (achatOkPourItemA) {
            System.out.println("L'achat de l'item " + item1.getName() + " est ok. L'item est déjà dans le storage et son stock est augmenté.");
            System.out.println("Le shop dispose de " + magasin.getCash() + " euros.");
        } else {
            System.out.println("L'achat de l'item " + item1.getName() + " n'est pas possible, pas assez d'argent");
            System.out.println("Le shop dispose de " + magasin.getCash() + " euros.");
        }
        System.out.println();

        // pour un nouvel item, à ajouter au storage
        boolean achatOkPourItemB = magasin.buy(item3);
        if (achatOkPourItemB) {
            System.out.println("L'achat de l'item " + item3.getName() + " est ok. L'item est nouveau et ajouté au storage.");
            System.out.println("Le shop dispose de " + magasin.getCash() + " euros.");
        } else {
            System.out.println("L'achat de l'item " + item3.getName() + " n'est pas possible, pas assez d'argent");
            System.out.println("Le shop dispose de " + magasin.getCash() + " euros.");
        }
    }
}
